package com.example.rotfl.kpov11;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.rotfl.kpov11.AdapterClass.InfOSamochodzieClass;
import com.example.rotfl.kpov11.AdapterClass.SamochodyAdapter;
import com.example.rotfl.kpov11.AdapterClass.SamochodyClass;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class CarsActivity extends AppCompatActivity {


    ConfigClass cc = new ConfigClass();
    LogActivity la = new LogActivity();

    String      imieNazwisko;
    String      bezReklamy;
    String      idWlasciciela;
    String      idSamochodu;
    int         wielkoscTabblicy;
    int         progresSucces;
    int         postep;

    ProgressDialog progressdialog;


    Context context = this;
    ArrayList<SamochodyClass> samochodyClasses;
    ArrayList<String> datyTrasy = new ArrayList<>();
    AlertDialog.Builder alertDialog;
    CharSequence[] items;

    SamochodyAdapter adapter;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);

        Intent i = getIntent();
        final Bundle przekazDane = i.getExtras();
        idWlasciciela = przekazDane.getString("idWlasciciela");


        pobierzSamochdy(idWlasciciela);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                idSamochodu = samochodyClasses.get(position).getIdSamochodu();
                datyTrasy.clear();
                pobierzDateTrasy(samochodyClasses.get(position).getIdSamochodu());

                //wpisanie do dialog wszystkich dat z listy datyTrasy

                alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Wybierz datę:");  //tytul alert dialog

            }
        });


//************* Linie odpowiadają za Snackbar - wyswietlane kropki różowej *************************
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Jesteś zalogowany jako : " +imieNazwisko, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
//**************************************************************************************************
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
//********************************* Metoda obsługująca górne menu **********************************
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//*************** Obsługa menu tu - przycisk Wyloguj się *******************************************
        if (id == R.id.action_settings) {
            idWlasciciela = "0";
            Intent i = new Intent(getApplicationContext(), LogActivity.class);
            startActivity(i);
            finish();
        }
        if(id == R.id.action_workers){
            Intent i = new Intent(getApplicationContext(), WorkerActivity.class);
            i.putExtra("idWlasciciela", idWlasciciela);
            startActivity(i);
        }
//**************************************************************************************************
        return super.onOptionsItemSelected(item);
    }

//**************************************************************************************************
//***************** Metoda jest odpowiedzialna za wyświetlenie *************************************
//****************** wszystkich samochodówdanego użytkownika ***************************************
    public void pobierzSamochdy(final String idWl){
        AsyncHttpClient client1 = new AsyncHttpClient();
        String url = cc.getIpAdress()+"/selectSamochody.php?idWlasciciela="+idWl;



        System.out.println("url pobierz samochody : " + url);
        samochodyClasses = new ArrayList<>();
        //wypisanie wszystkich samochodów w CarsActivity
        client1.get(CarsActivity.this, url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                InputStream inputStream = new ByteArrayInputStream(responseBody);

                try {
                    inputStream.read();
                    inputStream.close();

                    String text = new String(responseBody);
                    System.out.println(text);
                    Gson gson = new Gson();
                    com.google.gson.JsonParser jsonParser = new com.google.gson.JsonParser();
                    utnijReklame(text);
                    JsonArray jsonArray = jsonParser.parse(bezReklamy).getAsJsonArray();

                    for (JsonElement jsonElement : jsonArray) {
                        SamochodyClass samochody = gson.fromJson(jsonElement, SamochodyClass.class);
                            samochodyClasses.add(samochody);
                    }
                    adapter = new SamochodyAdapter(getApplicationContext(), samochodyClasses);
                    for (SamochodyClass s : samochodyClasses) {
                    }
                    imieNazwisko =  samochodyClasses.get(0).getImie() + " " + samochodyClasses.get(0).getNazwisko();
                    adapter = new SamochodyAdapter(getBaseContext(), samochodyClasses);
                    listView.setAdapter(adapter);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }

        });
    }
//**************************************************************************************************
//********* Metoda ma zapisać do listy dni w których rejestrowana była trasa************************
    public void pobierzDateTrasy(final String idSamochodu){


        AsyncHttpClient client1 = new AsyncHttpClient();
        String url = cc.getIpAdress()+"/selectDataTrasy.php?idSamochodu="+idSamochodu;
        //pobranie wszystkich dat w ktorych odbyła sie rejestracja
        client1.get(CarsActivity.this, url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                InputStream inputStream = new ByteArrayInputStream(responseBody);
                int i =0;
                try {
                    inputStream.read();
                    inputStream.close();
                    String text = new String(responseBody);
                    Gson gson = new Gson();
                    com.google.gson.JsonParser jsonParser = new com.google.gson.JsonParser();
                    utnijReklame(text);
                    JsonArray jsonArray = jsonParser.parse(bezReklamy).getAsJsonArray();

                    progresSucces = jsonArray.size();

                    if(i==0){
                        progressMethod();
                    }

                    //jeśli jsonArray jest rowna 0 wyswietla sie komunikat o braku zapisanych tras
                    if(jsonArray.size()==0){
                        alertDialog.setTitle("Nie masz zapisanej trasy");
                        alertDialog.show();
                    }
                    //******************************************************************************
                    for (JsonElement jsonElement : jsonArray) {
                        InfOSamochodzieClass daty = gson.fromJson(jsonElement, InfOSamochodzieClass.class);

                        wielkoscTabblicy=jsonArray.size();

                        if(datyTrasy.size()==0){
                            datyTrasy.add(daty.getDataRejestracji().substring(0, 10));
                       }
                       if(datyTrasy.get(datyTrasy.size()-1).equals(daty.getDataRejestracji().substring(0, 10).toString())) {
                           //to jest pomocnicze do else
                       } else{
                            datyTrasy.add(daty.getDataRejestracji().substring(0, 10).toString());
                       }
                       i++;
                        postep = i;



        //******************** Jeśli i==wielkosci listy dat wtedy zostaje wyswietlony dialog z datami ******************
        //******************************** w ktorych odbyla sie trasa **************************************************
                       if(i==jsonArray.size()){
                           items = datyTrasy.toArray(new CharSequence[datyTrasy.size()]);  //wpisanie wartości z listy do alert dialog
                           alertDialog.setItems(items, new AlertDialog.OnClickListener() {
                               @Override
                               public void onClick(DialogInterface dialog, int which) {

                                   Intent i = new Intent(getApplicationContext(), Trasa.class);
                                   i.putExtra("idWlasciciela", idWlasciciela);
                                   i.putExtra("idSamochodu", idSamochodu);
                                   i.putExtra("dataTrasy", datyTrasy.get(which));  //przeslanie wybranej daty do Trasy activity - na mapie dzieki
                                   // temu wyswietli sie trasa z danego dnia
                                   startActivity(i);
                               }
                           });
                           alertDialog.show();
                       }

        //**************************************************************************************************************************************
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }

        });
    }

//**************************************************************************************************


//************************ Metoda ucina końcówkę z reklamą darmowego serwera ***********************
//**************************************** www *****************************************************
    public void utnijReklame(String textResponseBody ){
        int pocztekSmieci = textResponseBody.indexOf("<");
        bezReklamy =  textResponseBody.substring(0,pocztekSmieci);
    }
//**************************************************************************************************
public void progressMethod(){
        progressdialog = new ProgressDialog(this);
        progressdialog.setMessage("Wczytywanie");
        progressdialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressdialog.setIndeterminate(true);
progressdialog.show();
        final Thread t = new Thread(){
         public  void run(){
             while (postep < progresSucces){
                 progressdialog.setProgress(postep);
             }
         }
        };
        t.start();
    }
}
