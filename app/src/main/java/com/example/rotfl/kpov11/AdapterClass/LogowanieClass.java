package com.example.rotfl.kpov11.AdapterClass;

/**
 * Created by rotfl on 02.12.2015.
 *
 * Klasa zawiera pola potrzebne do zalogowania do nadajnika
 * przez własciciela pojazdu
 */
public class LogowanieClass {


    /**
     * nrRejestracyjny : GND 7ABD
     * haslo : l
     */

    private String nrRejestracyjny;
    private String haslo;
    private String idSamochodu;
    private String idWlasciciela;
    private String imie;
    private String nazwisko;

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getIdWlasciciela() {
        return idWlasciciela;
    }

    public void setIdWlasciciela(String idWlasciciela) {
        this.idWlasciciela = idWlasciciela;
    }



    public void setNrRejestracyjny(String nrRejestracyjny) {
        this.nrRejestracyjny = nrRejestracyjny;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }

    public String getNrRejestracyjny() {
        return nrRejestracyjny;
    }

    public String getHaslo() {
        return haslo;
    }

    public String getIdSamochodu() {
        return idSamochodu;
    }

    public void setIdSamochodu(String idSamochodu) {
        this.idSamochodu = idSamochodu;
    }
}
