package com.example.rotfl.kpov11.AdapterClass;

/**
 * Created by rotfl on 20.01.2016.
 */
public class InfOSamochodzieClass {


    /**
     * idSamochodu : 2
     * id : 2481
     * dlugosc : 18.94929392
     * szerokosc : 53.28358454
     * predkosc : 0
     * odleglosc : 0
     * dataRejestracji : 2016-02-24 13:49:01
     * idWlasciciela : 2
     * imie : Tomasz
     * nazwisko : Kowalski
     */

    private String idSamochodu;
    private String id;
    private String dlugosc;
    private String szerokosc;
    private String predkosc;
    private String odleglosc;
    private String dataRejestracji;
    private String idWlasciciela;
    private String imie;
    private String nazwisko;

    public void setIdSamochodu(String idSamochodu) {
        this.idSamochodu = idSamochodu;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDlugosc(String dlugosc) {
        this.dlugosc = dlugosc;
    }

    public void setSzerokosc(String szerokosc) {
        this.szerokosc = szerokosc;
    }

    public void setPredkosc(String predkosc) {
        this.predkosc = predkosc;
    }

    public void setOdleglosc(String odleglosc) {
        this.odleglosc = odleglosc;
    }

    public void setDataRejestracji(String dataRejestracji) {
        this.dataRejestracji = dataRejestracji;
    }

    public void setIdWlasciciela(String idWlasciciela) {
        this.idWlasciciela = idWlasciciela;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getIdSamochodu() {
        return idSamochodu;
    }

    public String getId() {
        return id;
    }

    public String getDlugosc() {
        return dlugosc;
    }

    public String getSzerokosc() {
        return szerokosc;
    }

    public String getPredkosc() {
        return predkosc;
    }

    public String getOdleglosc() {
        return odleglosc;
    }

    public String getDataRejestracji() {
        return dataRejestracji;
    }

    public String getIdWlasciciela() {
        return idWlasciciela;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }
}
