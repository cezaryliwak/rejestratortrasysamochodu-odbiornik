package com.example.rotfl.kpov11.AdapterClass;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rotfl.kpov11.R;

import java.util.ArrayList;

/**
 * Created by rotfl on 15.03.2016.
 */
public class PracownicyAdapter extends BaseAdapter {
    Context context;
    ArrayList<PracownicyClass> listaPracownikow;

    View rowView;
    TextView imie;
    TextView nazwisko;

    public PracownicyAdapter(Context applicationContext, ArrayList<PracownicyClass> pracownicyClasses) {
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ArrayList<PracownicyClass> getListaPracownikow() {
        return listaPracownikow;
    }

    public void setListaPracownikow(ArrayList<PracownicyClass> listaSamochodow) {
        this.listaPracownikow = listaSamochodow;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        System.out.println("jestem mu w getView");
        rowView = inflater.inflate(R.layout.cars_elementy_listview, parent, false);

        PracownicyClass pracownicy = (PracownicyClass) getItem(position);


        imie = (TextView) rowView.findViewById(R.id.imie);
        nazwisko = (TextView) rowView.findViewById(R.id.nazwisko);

        imie.setText(pracownicy.getImie());
        nazwisko.setText(pracownicy.getNazwisko());



        return rowView;
    }
}
