package com.example.rotfl.kpov11.AdapterClass;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by rotfl on 20.01.2016.
 */
public class InfOSamochodzieAdapter extends BaseAdapter {

    Context context;
    ArrayList<InfOSamochodzieClass> info;

    public InfOSamochodzieAdapter(Context context, ArrayList<InfOSamochodzieClass> info) {
        this.context = context;
        this.info = info;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
