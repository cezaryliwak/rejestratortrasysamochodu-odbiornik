package com.example.rotfl.kpov11;
/**
 * Błąd - trzeba poprawić, ponieważ przy drugim otworzeniu snackbar aktywność z mapą się zamyka
 *
 * */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

import com.example.rotfl.kpov11.AdapterClass.InfOSamochodzieAdapter;
import com.example.rotfl.kpov11.AdapterClass.InfOSamochodzieClass;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Trasa extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private String bezReklamy;
    private String idSamochodu;
    private String dataTrasy;
    private double  dlugoscTrasy=0;

    private Double pLat;
    private Double pLng;
    private Double nLat;
    private Double nLng;

    private Date dataRTrasy;
    private Date dataZTrasy;
    private double sredniaPredkosc;
    private int hour;
    private int minute;
    private int seckond;
    private int miliseckond;


    Context context = this;
    TextView dialogText;
    InfOSamochodzieAdapter adapter;
    ConfigClass cc = new ConfigClass();
    ArrayList<InfOSamochodzieClass> infOSamochodzieClasses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trasa);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent i = getIntent();
        final Bundle przekazDane = i.getExtras();
        dataTrasy = przekazDane.getString("dataTrasy");
        idSamochodu = przekazDane.getString("idSamochodu");

        System.out.println("xxxxxxxxxxxxxxx " + dataTrasy);

        AsyncHttpClient client1 = new AsyncHttpClient();
        String url = cc.getIpAdress()+"/selectInfoSamochodzie.php?idSamochodu="+idSamochodu+"&dataTrasy="+dataTrasy;

        infOSamochodzieClasses = new ArrayList<>();
        System.out.println("asdasd " + url);
/*******************Rysowanie trasy ****************************************************************/
        client1.get(Trasa.this, url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                InputStream inputStream = new ByteArrayInputStream(responseBody);
                try {
                    inputStream.read();
                    inputStream.close();

                    String text = new String(responseBody);
                    utnijReklame(text);
                    Gson gson = new Gson();
                    JsonParser jsonParser = new JsonParser();
                    JsonArray jsonArray = jsonParser.parse(bezReklamy).getAsJsonArray();

                    for (JsonElement jsonElement : jsonArray) {
                        InfOSamochodzieClass info = gson.fromJson(jsonElement, InfOSamochodzieClass.class);
                        infOSamochodzieClasses.add(info);
                    }
//*************************************************************************************************************************************
//********************* rysowanie trasy na podstawie danych z bazy w której są zapisane współrzędne **************************
                    for (int i = 0; i < infOSamochodzieClasses.size(); i++) {

                        dlugoscTrasy += Double.parseDouble(infOSamochodzieClasses.get(i).getOdleglosc().toString());

                        if (i < infOSamochodzieClasses.size() - 1) {
                            try {
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  //ustawienie formatu daty

                                int iloscElementow = infOSamochodzieClasses.size();


                                String sdataRTrasy = infOSamochodzieClasses.get(0).getDataRejestracji();                    //pierwszy element z listy - tu będzie stał pierwszy marker ( miejsce w którym rozpoczęto trasę)
                                String sdataZTrasy = infOSamochodzieClasses.get(iloscElementow - 1).getDataRejestracji();   //pierwszy element z listy - tu będzie stał ostatni marker ( miejsce w którym zakończono trasę)

                                dataRTrasy = dateFormat.parse(sdataRTrasy);    //convert String to date
                                dataZTrasy = dateFormat.parse(sdataZTrasy);    //convert string to date

                                miliseckond = Math.round(dataZTrasy.getTime() - dataRTrasy.getTime());  //obliczanie czasu przebytej drogi podane w jesdnostce milisekund

                                //sredniaPredkosc += Double.parseDouble(infOSamochodzieClasses.get(i).getPredkosc());          //zsumowanie wszystkich predkosci z danego dnia - potrzebne do późniejszego obliczenia sredniej predkosci

                                sredniaPredkosc = dlugoscTrasy/miliseckond;

                                zamienJednostke(miliseckond);


                                pLat = Double.parseDouble(infOSamochodzieClasses.get(i).getSzerokosc());
                                pLng = Double.parseDouble(infOSamochodzieClasses.get(i).getDlugosc());

                                nLat = Double.parseDouble(infOSamochodzieClasses.get(i + 1).getSzerokosc());
                                nLng = Double.parseDouble(infOSamochodzieClasses.get(i + 1).getDlugosc());

                                LatLng punktP = new LatLng(pLat, pLng);      //punkt wczesniejszy - potrzebny do rysowania trasy samochodu
                                LatLng punktN = new LatLng(nLat, nLng);      //punkt późiejszy    - potrzebny do rysowaia trasy samochodu

                                Polyline line = mMap.addPolyline(new PolylineOptions()
                                        .add(punktP, punktN)                //rysowanie kawałka trasy (od, do)
                                        .width(7)                           //grubość rysowanej linii
                                        .color(Color.RED));                 //kolor rysowaej linii
//*********************** Ustawienie markera w miejscu startu **************************************
                                if (i == 1) {
                                    mMap.addMarker(new MarkerOptions()
                                            .position(punktP)                                   //ustewienie punktu w którym ma znaleźć się marker
                                            .title("Tu rozpoczęto trasę")                       //tytuł markera
                                            .snippet("Czas startu: "                            //opis markera
                                                            + infOSamochodzieClasses.get(i).getDataRejestracji()
                                            ));

                                }
//**************************************************************************************************
//*********************** Ustawienie markera w miejscu zakończenia *********************************
                                if (i == infOSamochodzieClasses.size() - 2) {
                                    mMap.addMarker(new MarkerOptions()
                                                    .position(punktN)
                                                    .title("Tu zakończono trasę")
                                    );
                                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(punktN,10));
                                }
//**************************************************************************************************
                            } catch (Exception e) {
                                System.out.println("tu jest błąd");
                            }
                        }

                    }

//*************************************************************************************************************************************
                    adapter = new InfOSamochodzieAdapter(getApplicationContext(), infOSamochodzieClasses);

                    sredniaPredkosc = sredniaPredkosc/infOSamochodzieClasses.size();

                    //zaokrąglenie liiczb do jednego miejsca po przecinku ***********
                    sredniaPredkosc *=10;
                    sredniaPredkosc = Math.round(sredniaPredkosc);

                    //**************************************************************
//************************* Akcja obsługująca reakcję na dotyk markera na mapie *******************************
                    mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {

                            if (marker.getTitle().equals("Tu zakończono trasę")) {

                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Informacje");
                                builder.setMessage(
                                        "Data i godzina rozpoczęcia trasy: \n"
                                        +infOSamochodzieClasses.get(0).getDataRejestracji()
                                        +"\nData i godzina zakończenia trasy: \n "
                                        + infOSamochodzieClasses.get(infOSamochodzieClasses.size() - 1).getDataRejestracji()
                                        + "\nCzas podróży: \t\t"
                                        + hour + " h " + minute + " m " + seckond + " s "
                                        + "\nSrednia prędkość: \t"
                                        + Double.toString(sredniaPredkosc/10) + " km/h"
                                        +"\nPrzebyta droga: \t\t"
                                        +dlugoscTrasy + "m");
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                    }
                                });
                                builder.show();

//                                snackbar = Snackbar
//                                        .make(layout,"",Snackbar.LENGTH_LONG);      //---------- Stworzenie nowego snack bar---------------
//                                snackbar.setDuration(10000);
//                                layout = snackbar.getView();
//                                snackText = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
//                                snackText.setTextColor(Color.WHITE);
//                                snackbar.setText("Zakończono trasę: \t "
//                                        + infOSamochodzieClasses.get(infOSamochodzieClasses.size() - 1).getDataRejestracji()
//                                       // + "\nCzas podróży: \t\t"
//                                      //  + hour + " h " + minute + " m " + seckond + " s \n"
//                                        + "\nSrednia prędkość: \t\t"
//                                        + Double.toString(sredniaPredkosc / infOSamochodzieClasses.size()));
//                                snackbar.show();                // wyswietlenie snackbar'a
                            }
                            if(marker.getTitle().equals("Tu rozpoczęto trasę")){
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Rozpoczęcie trasy");
                                builder.setMessage(
                                        "Pracownik: \n"
                                        +infOSamochodzieClasses.get(0).getImie() + " " + infOSamochodzieClasses.get(0).getNazwisko()
                                        +"\n Rozpoczęto: \n"
                                        +infOSamochodzieClasses.get(0).getDataRejestracji()
                                );
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                    }
                                });
                                builder.show();
                            }
                            return true;
                        }
                    });
//******************************************************************************************************************************
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            }
        });
/***************************************************************************************************/


    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        Location location = new Location(Context.LOCATION_SERVICE);

        mMap.addPolyline(new PolylineOptions().add(new LatLng(location.getLatitude(), location.getLongitude())));



    }

//*********************************** Zadaniem metody jest zamienienie jednostek na ****************
//***************************************** godziny, minuty i sekundy ******************************
    public void zamienJednostke(int miliseckond){
        hour = miliseckond/3600000;                 //obliczenie godziny
        miliseckond = miliseckond - 3600000 * hour;
        minute = miliseckond / 60000;               //obliczenie minuty
        miliseckond = miliseckond - 60000 * minute;
        seckond = miliseckond / 1000;               //obliczenie sekundy
    }


//************************ Metoda ucina końcówkę z reklamą darmowego serwera ***********************
//**************************************** www *****************************************************
    public void utnijReklame(String textResponseBody ){
        int pocztekSmieci = textResponseBody.indexOf("<");
        bezReklamy =  textResponseBody.substring(0,pocztekSmieci);
    }

//**************************************************************************************************

}
