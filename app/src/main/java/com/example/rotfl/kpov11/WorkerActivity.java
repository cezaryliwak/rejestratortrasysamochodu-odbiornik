package com.example.rotfl.kpov11;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.example.rotfl.kpov11.AdapterClass.PracownicyAdapter;
import com.example.rotfl.kpov11.AdapterClass.PracownicyClass;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by rotfl on 15.03.2016.
 */
public class WorkerActivity extends Activity {

    String bezReklamy;
    String idWlasciciela;

    ConfigClass cc = new ConfigClass();

    ListView listView;
    PracownicyAdapter pracownicyAdapter;
    ArrayList<PracownicyClass> pracownicyClasses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.worker_layout);

        listView = (ListView) findViewById(R.id.listView2);

        Intent i = getIntent();
        Bundle pobierzDane = i.getExtras();
        idWlasciciela = pobierzDane.getString("idWlasciciela");
        pobierzPracownikow(idWlasciciela);


    }

    public void pobierzPracownikow(final String idWl){
        AsyncHttpClient client1 = new AsyncHttpClient();
        String url = cc.getIpAdress()+"/selectPracownicy.php?idWlasciciela="+idWl;
        System.out.println("url pobierz samochody : " + url);
        pracownicyClasses = new ArrayList<>();
        //wypisanie wszystkich samochodów w CarsActivity
        client1.get(WorkerActivity.this, url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                InputStream inputStream = new ByteArrayInputStream(responseBody);

                try {
                    inputStream.read();
                    inputStream.close();

                    String text = new String(responseBody);
                    System.out.println(text);
                    Gson gson = new Gson();
                    com.google.gson.JsonParser jsonParser = new com.google.gson.JsonParser();
                    utnijReklame(text);
                    JsonArray jsonArray = jsonParser.parse(bezReklamy).getAsJsonArray();
System.out.println("bnnnnnn  : " + bezReklamy);
                    for (JsonElement jsonElement : jsonArray) {
                        PracownicyClass pracownicy = gson.fromJson(jsonElement, PracownicyClass.class);
                            pracownicyClasses.add(pracownicy);
                    }
                    pracownicyAdapter = new PracownicyAdapter(getApplicationContext(), pracownicyClasses);
                    for (PracownicyClass s : pracownicyClasses) {
                    }
                    pracownicyAdapter = new PracownicyAdapter(getBaseContext(), pracownicyClasses);
                    listView.setAdapter(pracownicyAdapter);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }

        });
    }
    //************************ Metoda ucina końcówkę z reklamą darmowego serwera ***********************
//**************************************** www *****************************************************
    public void utnijReklame(String textResponseBody ){
        int pocztekSmieci = textResponseBody.indexOf("<");
        bezReklamy =  textResponseBody.substring(0,pocztekSmieci);
    }
//**************************************************************************************************
}
