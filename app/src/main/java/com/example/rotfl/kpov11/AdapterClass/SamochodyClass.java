package com.example.rotfl.kpov11.AdapterClass;

/**
 * Created by rotfl on 20.01.2016.
 */
public class SamochodyClass {

    /**
     * idSamochodu : 2
     * idWlasciciela : 2
     * marka : Volkswagen Passat
     * nrRejestracyjny : NE
     * url : http://vignette3.wikia.nocookie.net/volkswagen/images/3/3d/Vw_passat.jpg/revision/latest?cb=20121012050139&path-prefix=pl
     * imie : Tomasz
     * nazwisko : Kowalski
     */

    private String idSamochodu;
    private String idWlasciciela;
    private String marka;
    private String nrRejestracyjny;
    private String url;
    private String imie;
    private String nazwisko;

    public void setIdSamochodu(String idSamochodu) {
        this.idSamochodu = idSamochodu;
    }

    public void setIdWlasciciela(String idWlasciciela) {
        this.idWlasciciela = idWlasciciela;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public void setNrRejestracyjny(String nrRejestracyjny) {
        this.nrRejestracyjny = nrRejestracyjny;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getIdSamochodu() {
        return idSamochodu;
    }

    public String getIdWlasciciela() {
        return idWlasciciela;
    }

    public String getMarka() {
        return marka;
    }

    public String getNrRejestracyjny() {
        return nrRejestracyjny;
    }

    public String getUrl() {
        return url;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }
}
