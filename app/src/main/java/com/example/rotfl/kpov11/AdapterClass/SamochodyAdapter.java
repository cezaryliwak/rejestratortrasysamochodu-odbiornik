package com.example.rotfl.kpov11.AdapterClass;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rotfl.kpov11.CarsActivity;
import com.example.rotfl.kpov11.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by rotfl on 20.01.2016.
 */
public class SamochodyAdapter extends BaseAdapter {
    Context context;
    ArrayList<SamochodyClass> listaSamochodow;
    CarsActivity main = new CarsActivity();
    TextView rejestr;
    TextView marka;
     View rowView;

    public SamochodyAdapter(Context context, ArrayList<SamochodyClass> listaSamochodow) {
        this.context = context;
        this.listaSamochodow = listaSamochodow;
    }

    @Override
    public int getCount() {
        return listaSamochodow.size();
    }

    @Override
    public Object getItem(int position) {
        return listaSamochodow.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);

         rowView = inflater.inflate(R.layout.cars_elementy_listview, parent, false);
         SamochodyClass samochody = (SamochodyClass) getItem(position);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.imageView);

        Picasso.with(context).load(samochody.getUrl()).into(imageView);
         rejestr = (TextView) rowView.findViewById(R.id.rejestracja);
         marka = (TextView) rowView.findViewById(R.id.marka);

        rejestr.setText(samochody.getNrRejestracyjny());
        marka.setText(samochody.getMarka());



        return rowView;
    }
}
