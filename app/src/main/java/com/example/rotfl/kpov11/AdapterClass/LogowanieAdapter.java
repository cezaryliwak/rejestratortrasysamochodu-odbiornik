package com.example.rotfl.kpov11.AdapterClass;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by rotfl on 02.12.2015.
 */
public class LogowanieAdapter extends BaseAdapter{

    Context context;
    ArrayList<LogowanieClass> logowanieClasses;

    public LogowanieAdapter(Context context, ArrayList<LogowanieClass> logowanieClasses) {
        this.context = context;
        this.logowanieClasses = logowanieClasses;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
