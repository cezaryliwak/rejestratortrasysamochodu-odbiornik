package com.example.rotfl.kpov11.AdapterClass;

/**
 * Created by rotfl on 15.03.2016.
 */
public class PracownicyClass {

    /**
     * idPracownika : 1
     * idWlasciciela : 1
     * imie : Andrzej
     * nazwisko : Baran
     * haslo : a
     */

    private String idPracownika;
    private String idWlasciciela;
    private String imie;
    private String nazwisko;
    private String haslo;

    public void setIdPracownika(String idPracownika) {
        this.idPracownika = idPracownika;
    }

    public void setIdWlasciciela(String idWlasciciela) {
        this.idWlasciciela = idWlasciciela;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }

    public String getIdPracownika() {
        return idPracownika;
    }

    public String getIdWlasciciela() {
        return idWlasciciela;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public String getHaslo() {
        return haslo;
    }
}
